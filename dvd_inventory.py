import sqlite3 as sql
import os
conn = sql.connect('DVDINVENTORY.sqlite')
cur = conn.cursor()

try:
    cur.execute('''CREATE TABLE DVD_INVENTORY (title text, star_name text, year text, genere text)''')
except:
    print "DATABSE WITH THIS NAME ALREADY EXISTS"
    pass


class Dvd:    
    def __init__(self):
        self.title = raw_input("\nEnter title of DVD: ")
        self.star_name = raw_input("\nEnter star name: ")
        self.year_of_release = raw_input("\nFormat of date: (dd/mm/yyyy) \nEnter year of release of DVD: ")
        self.genere = raw_input("\nSelect from Romance, Action, Drama, Horror \nEnter the genere: ")

    def __str__(self):
        print "Title: " + str(self.title.lower())
        print "Star name: " + str(star_name.lower())
        print "Year of release: " + str(year_of_release.lower())
        print "Genere :" + str(genere.lower)



class Inventory:
    def __init__(self):
        pass
    
    #Function to search an existing dvd from inventory
    def search_dvd_by_name(self, m = None):
        global flag
        flag = True
        #print "*** PLEASE ENTER THE DVD TITLE ONLY ***\n"
        #print " *** DO NOT ENTER GENERE OR STAR OR YEAR OF RELEASE *** "
        if m == None : m = raw_input("Enter name of the DVD: ")
        m = m.lower()
        c = 0
        x = 20
        for row in cur.execute('''SELECT * from DVD_INVENTORY where title = (?)''', (m,)):
            c += 1
            if c == 1:
                print "\nTITLE:".ljust(x+5)+"STAR".ljust(x)+"YEAR".ljust(x)+"GENERE".ljust(x)
            print row[0].ljust(x + 5),row[1].ljust(x),row[2].ljust(x),row[3].ljust(x)

        if c == 0:
            print "No DVD with this title.."
            flag = False

    #function to search all dvd with the given genere
    def search_dvd_by_genere(self, m = None):
        """print "*** PLEASE ENTER DVD GENERE ONLY \n"
        print " *** DO NOT ENTER TITLE OR STAR OR YEAR OF RELEASE *** " """
        if m == None: m = raw_input("Enter genere of the DVD: ")
        m = m.lower()
        c = 0
        x = 20
        for row in cur.execute('''SELECT * from DVD_INVENTORY where genere = (?)''', (m,)):
            c += 1
            if c == 1:
                print "\nTITLE:".ljust(x+5)+"STAR".ljust(x)+"YEAR".ljust(x)+"GENERE".ljust(x)
            print row[0].ljust(x + 5),row[1].ljust(x),row[2].ljust(x),row[3].ljust(x)
        if c == 0:
            print "No DVD with this Genere.."
            flag = False
            
    #function to add new dvd
    def add_new_dvd(self):
        d = Dvd()
        cur.execute('''INSERT INTO DVD_INVENTORY VALUES(?,?,?,?)''', (d.title, d.star_name, d.year_of_release, d.genere,))
        conn.commit()

    #function to modify existing dvd
    def modify_existing_dvd(self, m = None):
        global flag
        flag = True
        if m == None: m = raw_input("Enter title of DVD to be edited: ")
        if m.lower() != "exit()":
            self.search_dvd_by_name(m)
            if flag == True:
                print "WHAT DO YOU WANT TO CHANGE"
                print "1. title                 (Press '1' or type: 'title')"
                print "2. star                  (Press '2' or type: 'star')"
                print "3. year of release       (Press '3' or type: 'year')"
                print "4. genere                (Press '4' or type: 'genere')"
                n = raw_input("Enter your choice: ")
                n = n.lower().strip()
                new_value = raw_input("Enter the new value for the field: ")
                new_value = new_value.lower()
                if n == "genere" or n == "4": cur.execute('''UPDATE DVD_INVENTORY SET genere = (?) WHERE title = (?)''', (new_value,m,))
                elif n == "star name" or n == "2": cur.execute('''UPDATE DVD_INVENTORY SET star_name = (?) WHERE title = (?)''', (new_value,m,))
                elif n == "title" or n == "1": cur.execute('''UPDATE DVD_INVENTORY SET title = (?) WHERE title = (?)''', (new_value,m,))
                elif n == "year of release" or n == "3": cur.execute('''UPDATE DVD_INVENTORY SET year = (?) WHERE title = (?)''', (new_value,m,))
                conn.commit()
            else:
                self.modify_existing_dvd()

    #function to delete an existing dvd
    def delete_dvd(self):
        m = raw_input("Enter DVD title fior deleting: ")
        m = m.lower()
        if m != "exit()":
            cur.execute(''' DELETE FROM DVD_INVENTORY WHERE title = (?) ''', (m,))
            print "\n\n"
            conn.commit()
        
    #function to display the inventory
    def display(self):
        try:
            x  = 20
            print "\nTITLE:".ljust(x+5)+"STAR".ljust(x)+"YEAR".ljust(x)+"GENERE".ljust(x)
            for row in cur.execute('''SELECT * FROM DVD_INVENTORY'''):
                print row[0].ljust(x + 5),row[1].ljust(x),row[2].ljust(x),row[3].ljust(x)
        except:
            print "EMPTY DATABASE"

    #function to drop the current inventory
    def empty_database(self):
        cur.execute('''DROP TABLE DVD_INVENTORY''')
        print "\n\n"
        conn.commit()
        
i = Inventory()

def func():
    print "WELCOME TO THE INVENTORY"
    print "WHAT DO YOU WANT TO DO:\n"
    print "1. DISPLAY THE INVENTORY    (PRESS 1 OR TYPE: 'display inventory')"
    print "2. DISPLAY AN EXISTING DVD  (PRESS 2 OR TYPE: 'display dvd')"
    print "3. ADD NEW DVD              (PRESS 3 OR TYPE: 'add')"
    print "4. DELETE EXISTING DVD      (PRESS 4 OR TYPE: 'delete')"
    print "5. MODIFY EXISTING DVD      (PRESS 5 OR TYPE: 'modify')"
    inp = raw_input("Enter your choice: ").lower().strip()
    if inp != "exit()":
        if inp in ["1","2","3","4","5","diplay inventory","display dvd","modify","add","delete"]:
            if inp == "1" or inp == "display inventory":
                i.display()
                print "\n"
                func()
            elif inp == "3" or inp == "add":
                i.add_new_dvd()
                print "\n"
                func()
            elif inp == "2" or inp == "display dvd":
                print "SEARCH BY TITLE (TYPE: 'name')"
                print "SEARCH BY GENERE (TYPE: 'genere')"
                l = raw_input("Enter your choice: ").lower().strip()
                if l == "name":
                    i.search_dvd_by_name()
                    print"\n"
                    func()
                elif l == "genere":
                    i.search_dvd_by_genere()
                    print "\n"
                    func()
                else:
                    print "Enter correct choice.."
                    func()
            elif inp == "4" or inp == "delete":
                i.delete_dvd()
                print "\n"
                func()
            elif inp == "5" or inp == "modify":
                i.modify_existing_dvd()
                print "\n"
                func()
        else:
            print "\nEnter a valid selection..\n"
            func()

func()
conn.commit()
